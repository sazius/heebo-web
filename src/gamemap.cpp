#include "gamemap.h"

#include <sstream>
#include <iostream>

//------------------------------------------------------------------------------

GameMap::GameMap(int width, int height) :
  m_width(width), m_height(height)
{}

//------------------------------------------------------------------------------

bool GameMap::pointOK(int r, int c) const {
  return r >= 0 && r < m_height && c >= 0 && c < m_width;
}

//------------------------------------------------------------------------------

char GameMap::at(int r, int c) const {
  if (!pointOK(r, c)) {
    return ' ';
  }
  return m_map[r][c];
}

//------------------------------------------------------------------------------

bool GameMap::isBlocking(int r, int c) const {
  if (!pointOK(r, c))
    return true;
  return at(r, c) == 'W';
}

//------------------------------------------------------------------------------

void GameMap::setProperty(int r, int c, const string& s) {
  if (!pointOK(r, c)) {
    cout << "ERROR setting property at " << r << "," << c
         << ": coordinate is not valid!" << endl;
    return;
  }
  if (!s.empty())
    m_prop[make_pair(r, c)] = s;
}

//------------------------------------------------------------------------------

void GameMap::load(ifstream& fp) {
  int n = 0;
  string line;
  while (getline(fp, line)) {
    if (line[0] == '#') {
      int p = line.find(" ", 2);
      string word = line.substr(2, p-2);

      // Property-line looks like this: # property: 5,4 locked
      // We parse "5,4" -> rc -> r and c
      // "locked" -> property
      if (word == "property:") {
        int q = line.find(" ", p+1);
        string rc = line.substr(p+1, q-p-1);
        string property = line.substr(q+1);

        p = rc.find(",");
        int r = stoi(rc.substr(0, p));
        int c = stoi(rc.substr(p+1));

        setProperty(r, c, property);
      }
        
      continue;
    }

    n++; // count uncommented lines

    if (line.length() != (size_t)m_width) {
      cout << "Line length:" << line.length() << "!=" << m_width;
      return;
    }

    const size_t p = line.find_first_not_of(ALLOWED_MAP_CHARS);
    if (p != string::npos) {
      cout << "Character" << line.substr(p, 1) << "on line" << n
           << "is not an allowed map character." << endl;
      return;
    }

    vector<char> list;
    for (int i=0; i<m_width; i++) {
      char ch = line.at(i);
      list.push_back(ch);
    }
    m_map.push_back(list);

    if (n == m_height) 
      break;
  }      
}

//------------------------------------------------------------------------------

GameMap* GameMap::fromStream(ifstream& fp, int w, int h) {
  GameMap* gm = new GameMap(w,h);
  gm->load(fp);
  return gm;
}
