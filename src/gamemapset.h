#ifndef _GAMEMAPSET_H_
#define _GAMEMAPSET_H_

#include <list>
#include <string>

#include "gamemap.h"

using namespace std;

class GameMapSet {
public:
  // explicit GameMapSet(int width, int height);
  explicit GameMapSet(const string& fileName, int initialLevel);

  // void save(const QString& fileName="");
  
  const string& fileName() const { return m_fileName; }
  
  int level() const { return m_level; }
  int setLevel(int l);
  int numLevels() const { return m_number; }
  bool onLastLevel() const { return m_level == m_number-1; }

  // GameMap* newMap(int);
  // void removeMap(int);
  // void swapMaps(int, int);

  GameMap* map(int l) { return levelOK(l) ? m_maps.at(l) : NULL; }

  // const string& at(int r, int c) const;
  // const string& prop(int r, int c) const;

// signals:
//   void levelChanged();

private:
  void loadMap();

  bool levelOK(int l) { return l >= 0 && l < m_number; }

  string m_fileName;
  
  vector<GameMap*> m_maps;

  int m_width, m_height, m_number;
  int m_level; // current level
};

#endif /* _GAMEMAPSET_H_ */
