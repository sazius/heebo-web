#include "gamemapset.h"

#include <iostream>
#include <fstream>

//------------------------------------------------------------------------------

// GameMapSet::GameMapSet(int width, int height, QObject* parent) :
//   QObject(parent), m_width(width), m_height(height), m_number(0), m_level(-1)
// {
// }

//------------------------------------------------------------------------------

GameMapSet::GameMapSet(const string& fileName, int initialLevel) :
  m_fileName(fileName)
{
  loadMap();
  setLevel(initialLevel);
}

//------------------------------------------------------------------------------

int GameMapSet::setLevel(int l) {
  if (l != m_level && levelOK(l)) {
    m_level = l;
    //emit levelChanged();
  }

  return m_level;
}

//------------------------------------------------------------------------------

// const string& GameMapSet::at(int r, int c) const {
//   return m_maps[m_level]->atName(r,c);
// }

// //------------------------------------------------------------------------------

// const string& GameMapSet::prop(int r, int c) const {
//   return m_maps[m_level]->propertyName(r,c);
// }

//------------------------------------------------------------------------------

void GameMapSet::loadMap() {
  ifstream fp(m_fileName);

  if (!fp.is_open()) {
    cout << "ERROR: Cannot open map file: " << m_fileName << endl;
    return;
  }

  int n = 0;
  string line;
  while (getline(fp, line)) {
    if (line[0] == '#')
      continue;
    
    n++; // count only uncommented lines

    if (n==1)
      m_width = stoi(line);
    else if (n==2)
      m_height = stoi(line);
    else if (n==3) {
      m_number = stoi(line);
      break;
    }    
  }
  cout << "Loaded map " << m_fileName << " with size " << m_width << "x"
       << m_height << " and " << m_number << " maps." << endl;
  
  for (int i=0; i<m_number; i++) {
    GameMap* gm = GameMap::fromStream(fp, m_width, m_height);
    m_maps.push_back(gm);
  }
}

//------------------------------------------------------------------------------

// void GameMapSet::save(const QString& fileName) {
//   if (!fileName.isEmpty())
//     m_fileName = fileName;

//   QFile fp(m_fileName);
//   if (!fp.open(QIODevice::WriteOnly | QIODevice::Text)) {
//     qCritical() << "Unable to open" << m_fileName << "for opening.";
//     return;
//   }

//   QTextStream out(&fp);

//   out << "# Heebo game map set.\n";
//   out << "# width\n" << m_width << "\n";
//   out << "# height\n" << m_height << "\n";
//   out << "# number of maps\n" << m_number << "\n";

//   for (int i=0; i<m_number; i++) {
//     out << "# map" << i+1 << "\n";
//     m_maps[i]->save(out);
//   }
// }

//------------------------------------------------------------------------------

// GameMap* GameMapSet::newMap(int index) {
//   GameMap* gm = GameMap::emptyMap(m_width, m_height);
//   m_maps.insert(index, gm);
//   m_number++;
//   return gm;
// }

// //------------------------------------------------------------------------------

// void GameMapSet::removeMap(int index) {
//   m_maps.removeAt(index);
//   m_number--;
// }

// //------------------------------------------------------------------------------

// void GameMapSet::swapMaps(int i, int j) {
//   if (OK(i) && OK(j))
//     m_maps.swap(i, j);
// }
