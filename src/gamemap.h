#ifndef _GAMEMAP_H_
#define _GAMEMAP_H_

#include <fstream>
#include <map>
#include <vector>

#define ALLOWED_MAP_CHARS "012346789W|-<>AUO"

using namespace std;

//------------------------------------------------------------------------------

class GameMap {
public:
  static GameMap* fromStream(ifstream&, int, int);

  char at(int r, int c) const;
  bool isBlocking(int r, int c) const;

  void setProperty(int r, int c, const string& s);

  int width() const { return m_width; }
  int height() const { return m_height; }

private:
  explicit GameMap(int, int);
  void load(ifstream&);
  bool pointOK(int r, int c) const;

  vector< vector<char> > m_map;
  map< pair<int, int>, string > m_prop;
  int m_width, m_height;
};

#endif /* _GAMEMAP_H_ */
