#include "heebo.h"

#include "util.h"

//------------------------------------------------------------------------------

Heebo::Heebo(int type) :
  m_type(type), m_animStep(0), m_animDX(0), m_animDY(0),
  m_movingBack(false), m_doRemove(false)
{}

//------------------------------------------------------------------------------

void Heebo::startMove(int dx, int dy) {
  m_animStep = 0;
  m_animDX = dx;
  m_animDY = dy;
  m_movingBack = false;
}

//------------------------------------------------------------------------------

void Heebo::stopMove() {
  m_animStep = 0;
  m_animDX = 0;
  m_animDY = 0;
  m_movingBack = false;
}

//------------------------------------------------------------------------------

void Heebo::moveBack() {
  m_animDX = -m_animDX;
  m_animDY = -m_animDY;
  m_movingBack = true;
}

void Heebo::spawnMove() {
  m_animDX = 0;
  m_animDY = 1;
  m_animStep = BLOCK_HEIGHT;
  m_movingBack = true;
}

//------------------------------------------------------------------------------

int Heebo::offsetX() const {
  int dd = sign(m_animDX) * (m_movingBack ? -1 : 1);
  return m_animStep * dd;
}

//------------------------------------------------------------------------------

int Heebo::offsetY() const {
  int dd = sign(m_animDY) * (m_movingBack ? -1 : 1);
  return m_animStep * dd;
}

//------------------------------------------------------------------------------

// Returns true if movement should continue, false means to stop
bool Heebo::moveStep() {
  m_animStep += 8*(m_movingBack ? -1 : +1);

  const int moveMax = (abs(m_animDX) > 0 ? BLOCK_WIDTH : BLOCK_HEIGHT);

  if (m_movingBack)
    return m_animStep > 0;
  else
    return m_animStep < moveMax;
}

//------------------------------------------------------------------------------

string Heebo::typeName(int type) {
  if (type == 1)
    return "circle";
  else if (type == 2)
    return "polygon";
  else if (type == 3)
    return "square";
  else if (type == 4)
    return "triangle_down";
  else if (type == 5)
    return "triangle_up";
  return "";
}

