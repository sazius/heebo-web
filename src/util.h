#ifndef _UTIL_H_
#define _UTIL_H_

#define WINDOW_WIDTH 480
#define WINDOW_HEIGHT 820  // 720
#define TOOLBAR_HEIGHT 100

#define BLOCK_WIDTH 80
#define BLOCK_HEIGHT 80

#define MAX_HEEBO_TYPES 5

#define MOVE_THRES 5   // threshold for mouse move

//------------------------------------------------------------------------------

int sign(int x);

#endif /* _UTIL_H_ */
