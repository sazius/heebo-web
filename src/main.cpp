#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <emscripten.h>

#include <cmath>
#include <filesystem>
#include <iostream>
#include <random>
#include <sstream>
#include <string>

#include "util.h"
#include "heebo.h"
#include "gamemapset.h"
#include "gamemap.h"

using namespace std;

SDL_Window *window;
SDL_Renderer *renderer;

TTF_Font *mainFont = NULL;

random_device rd;
mt19937 rng(rd());
uniform_int_distribution<int> randomHeeboTypeDistr(1, MAX_HEEBO_TYPES);

GameMapSet* gameMapSet = NULL;
int currentLevel = 0;

const SDL_Color uiAccentColor = {216, 0, 216, 255};

int clickX = -1;
int clickY = -1;
bool hasChanged = true;
bool animRunning = false;


struct Sprite {
  // string fileName;
  SDL_Texture *texture;
  int width;
  int height;
};

typedef pair<int, int> boardPoint;

map<string, Sprite> sprites;
map<boardPoint, Heebo*> board;
map<boardPoint, bool> isGold;

//------------------------------------------------------------------------------

int reportError(int ret) {
  printf("Error from SDL: %s\n", SDL_GetError());
  return ret;
}

int reportImageError() {
  printf("Error loading image with SDL: %s\n", IMG_GetError());
  return -1;
}

void renderSprite(const Sprite &sprite, int x, int y) {
  SDL_Rect dest = {.x = x, .y = y, .w = sprite.width, .h = sprite.height};
  SDL_RenderCopy(renderer, sprite.texture, NULL, &dest);
}

//------------------------------------------------------------------------------

void generateToolbar() {
  // Generate gradient background for the bottom toolbar
  SDL_Surface *tbSurface = SDL_CreateRGBSurface(0, WINDOW_WIDTH, TOOLBAR_HEIGHT,
                                                32, 0, 0, 0, 0);
  SDL_Renderer *rend = SDL_CreateSoftwareRenderer(tbSurface);
  for (int j=0; j<TOOLBAR_HEIGHT; j++) {
    int c = 12 + round(48*j/TOOLBAR_HEIGHT);  // original heebo had 12 + 24
    SDL_SetRenderDrawColor(rend, c, c, c, SDL_ALPHA_OPAQUE);
    SDL_RenderDrawLine(rend, 0, j, WINDOW_WIDTH, j);
  }

  // sstring text = "Level " + (currentLevel+1) + "/" + gameMapSet->numLevels();
  ostringstream ss;
  ss << "Level " << currentLevel+1 << "/" << gameMapSet->numLevels();
  // string text = std::format("Level {}/{}", currentLevel+1, gameMapSet->numLevels());
  SDL_Surface* textSurface = TTF_RenderUTF8_Blended(mainFont, ss.str().c_str(),
                                                    uiAccentColor);
  const int margin = (TOOLBAR_HEIGHT-textSurface->h)/2;
  SDL_Rect rect = {margin, margin, textSurface->w, textSurface->h};
  SDL_BlitSurface(textSurface, NULL, tbSurface, &rect);
  
  SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, tbSurface);
  
  Sprite s = {texture, tbSurface->w, tbSurface->h};
  sprites["toolbar_bg"] = s;

  SDL_FreeSurface(tbSurface);
  SDL_FreeSurface(textSurface);
  SDL_DestroyRenderer(rend);
}

//------------------------------------------------------------------------------

void initBoard() {
  GameMap* map = gameMapSet->map(currentLevel);
  isGold.clear();
  for (auto& i: board) delete i.second;
  board.clear();

  generateToolbar();

  for (int r=0; r<map->height(); r++) {
    for (int c=0; c<map->width(); c++) {
      // char ch = map->at(r, c);

      if (!map->isBlocking(r, c)) {
        // Check if the two Heebos on the left are the same, don't
        // make a third one of the same type
        int skip1 = 0;
        const boardPoint leftPt = make_pair(r, c-1);
        const boardPoint leftLeftPt = make_pair(r, c-2);
        if (c > 1 && board.contains(leftPt) && board.contains(leftLeftPt)
            && board[leftPt]->type() == board[leftLeftPt]->type())
          skip1 = board[leftPt]->type();

        // Ditto for the two above
        int skip2 = 0;
        const boardPoint abovePt = make_pair(r-1, c);
        const boardPoint aboveAbovePt = make_pair(r-2, c);
        if (r > 1 && board.contains(abovePt) && board.contains(aboveAbovePt)
            && board[abovePt]->type() == board[aboveAbovePt]->type())
          skip2 = board[abovePt]->type();

        int type = 0;
        do {
          type = randomHeeboTypeDistr(rng);
        } while (type == skip1 || type == skip2);
        
        Heebo *heebo = new Heebo(type);
        board[make_pair(r,c)] = heebo;
      }
    }
  }
}


//------------------------------------------------------------------------------

// Clear (turn gold) random blocks where we have a Heebo of the given type
void clearRandomBlock(int blockType, int count) {
  // GameMap* map = gameMapSet->map(currentLevel);

  // Loop over all Heebos, add those without gold background and of
  // the right type to the list of clearable points
  vector<boardPoint> clearablePoints;
  for (auto it = board.cbegin(); it != board.cend(); it++) {
    auto pos = it->first;
    Heebo* heebo = it->second;
    bool gold = isGold.contains(pos) && isGold[pos];
    if (heebo->type() == blockType && !gold)
      clearablePoints.push_back(pos);
  }

  while (clearablePoints.size() > 0 && count > 0) {
    uniform_int_distribution<int> uni(0, clearablePoints.size()-1);
    int i = uni(rng);
    auto pos = clearablePoints.at(i);
    isGold[pos] = true;
    clearablePoints.erase(clearablePoints.begin() + i);
    count--;
  }
}  

//------------------------------------------------------------------------------

/* 
   Check one line (row/column) for subsequent heebos of same colour.
   j gives the row/column
   rows is true for checking row, false for column
   mark is true if we should actually remove heebos, false=just checking
*/
int checkSubsequentLine(int j, bool rows, bool mark) {
  GameMap* map = gameMapSet->map(currentLevel);

  int last_b = 0;  // type of previous b
  int count = 0;   // count subsequent heebos so far
  int imax = rows ? map->width() : map->height();  // max index for row/column
  int changes = 0;

  for (int i=0; i<imax; i++) {
    auto pos = rows ? make_pair(j, i) : make_pair(i, j);
    int b = 0;
    Heebo* obj = NULL;

    // if we're on a heebo, set b to its type (otherwise b=0)
    if (board.contains(pos)) {
      obj = board[pos];
      b = obj->type();
    }

    // count another heebo in sequence
    if (b != 0 && last_b == b)
      count++;

    // if heebo type changed, or we're at the last one
    if (last_b != b || i == imax-1) {
      if (count >= 2) {
        if (mark) { // if we should mark removals
          // set k_begin-k_end to span the subsequent heebos
          int k_begin = i-count-1;
          int k_end = i;
          if (last_b == b) {
            k_begin++;
            k_end++;
          }

          for (int k=k_begin; k<k_end; k++) {
            int r = rows ? j : k;
            int c = rows ? k : j;

            // FIXME
            // if (board[r][c].locked) {
            //   board[r][c].locked--;
            // }
            // if (!board[r][c].locked) {
            //   board[r][c].to_remove = true;
            //   bg_grid[r][c].cleared = true;
            // }
            auto pos = make_pair(r, c);
            if (board.contains(pos)) {
              board[pos]->markRemove();
              isGold[pos] = true;
            }
          }
          if (count >= 3)
            clearRandomBlock(last_b, count-2);
        } // mark
        changes++;
      } // count >= 2
      count = 0;
    } // if heebo type changed
    last_b = b;
  }
  return changes;
}

//------------------------------------------------------------------------------

bool checkSwitch(boardPoint pt1, boardPoint pt2) {
  if (pt1 == pt2) // sanity check
    return 1;
  
  int changes = 0;
  changes += checkSubsequentLine(pt1.second, false, false);
  changes += checkSubsequentLine(pt1.first, true, false);
  changes += checkSubsequentLine(pt2.second, false, false);
  changes += checkSubsequentLine(pt2.first, true, false);
  return changes > 0;
}

//------------------------------------------------------------------------------

int checkForSubsequentHeebos() {
  int changes = 0;
  GameMap* map = gameMapSet->map(currentLevel);

  for (int j=0; j<map->height(); j++)
    changes += checkSubsequentLine(j, true, true);

  for (int i=0; i<map->width(); i++)
    changes += checkSubsequentLine(i, false, true);

  if (changes == 0)
    return 0;
  
  for (auto it = board.cbegin(); it != board.cend();) {
    Heebo* heebo = it->second;
    if (heebo->toRemove()) {
      it = board.erase(it);
      delete heebo;
    } else {
      ++it;
    }
  }

  return changes;
}

//------------------------------------------------------------------------------

bool victoryCheck() {
  GameMap* map = gameMapSet->map(currentLevel);
  bool victory = true;

  // Loop over all map points until we find a block which should be
  // gold and is not, proving victory condition hasn't been reached
  for (int j=0; j<map->height() && victory; j++) {
    for (int i=0; i<map->width() && victory; i++) {
      auto pos = make_pair(j, i);
      victory = (isGold.contains(pos) && isGold[pos]) || map->isBlocking(j, i);
    }
  }
  return victory;
}
  

//------------------------------------------------------------------------------

int fallDown() {
  int changes = 0;
  GameMap* map = gameMapSet->map(currentLevel);

  for (int j=map->height()-2; j>=0; j--) {  // skip last row as it cannot fall down
    for (int i=0; i<map->width(); i++) {
      auto pos = make_pair(j, i);
      if (board.contains(pos)) {
        Heebo* heebo = board[pos];
        auto posBelow = make_pair(j+1, i);
        if (!board.contains(posBelow) && !map->isBlocking(j+1, i)) {
          heebo->startMove(0, 1);
          // cout << "FALLDOWN " << j << "," << i << endl;
          changes++;
        }
      }
    }
  }
  return changes;
}

//------------------------------------------------------------------------------

int spawnNewHeebos() {
  int changes = 0;
  GameMap* map = gameMapSet->map(currentLevel);

  for (int i=0; i<map->width(); i++) {
    auto pos = make_pair(0, i);
    if (!board.contains(pos) && !map->isBlocking(0, i)) {
      int type = randomHeeboTypeDistr(rng);
      Heebo* heebo = new Heebo(type);
      board[pos] = heebo;
      heebo->spawnMove();
      changes++;
    }
  }

  return changes;
}

//------------------------------------------------------------------------------

void drawScreen() {
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
  SDL_RenderClear(renderer);

  GameMap* map = gameMapSet->map(currentLevel);

  // Draw board
  for (int r=0; r<map->height(); r++) {
    for (int c=0; c<map->width(); c++) {
      char ch = map->at(r, c);

      auto pos = make_pair(r, c);
      bool gold = isGold.contains(pos) && isGold[pos];
      string bg_name = (gold ? "block_gold" : "bg");
      
      string im = "";
      if (ch == '0')
        im = bg_name;
      else if (ch == 'W')
        im = "block_wall";
      else if (ch == '|') 
        im = "wb_updown";
      else if (ch == '-')
        im = "wb_leftright";
      else if (ch == '<')
        im = "wb_deadend_left";
      else if (ch == '>')
        im = "wb_deadend_right";
      else
        im = "wb_" + string(1, ch);

      int x = c*BLOCK_WIDTH;
      int y = r*BLOCK_HEIGHT;

      if (im.starts_with("wb_")) {
        renderSprite(sprites[bg_name], x, y);
      }
      renderSprite(sprites[im], x, y); 
    }
  }

  // Draw Heebos
  for (int r=0; r<map->height(); r++) {
    for (int c=0; c<map->width(); c++) {
      auto pos = make_pair(r,c);
      int x = c*BLOCK_WIDTH;
      int y = r*BLOCK_HEIGHT;
      
      if (board.contains(pos)) {
        Heebo* heebo = board[pos];
        string spriteName = heebo->typeName();
        if (animRunning)
          spriteName += "_eyeshut";
        renderSprite(sprites[spriteName], x + heebo->offsetX(),
                     y + heebo->offsetY());
      }
    }
  }

  renderSprite(sprites["toolbar_bg"], 0, WINDOW_HEIGHT-TOOLBAR_HEIGHT);

  Sprite iconSprite = sprites["icon_menu"];
  int margin = (TOOLBAR_HEIGHT-iconSprite.height)/2;
  renderSprite(iconSprite, WINDOW_WIDTH-iconSprite.width-margin, 
               WINDOW_HEIGHT-iconSprite.height-margin);

  // Draw renderer
  SDL_RenderPresent(renderer);
}  

//------------------------------------------------------------------------------

void mainLoop() {
  animRunning = false;
  GameMap* map = gameMapSet->map(currentLevel);

  // Move any Heebos that are currently moving
  for (int j=0; j<map->height(); j++) {  // we have to loop over coords, as we're modifying
    for (int i=0; i<map->width(); i++) { // board (and cannot loop over it)
      auto pos = make_pair(j, i);
      if (board.contains(pos)) {
        Heebo* heebo = board[pos];
        if (heebo->isMoving()) {
          bool stop = !heebo->moveStep();
          hasChanged = true;

          if (stop) {
            auto newPos = make_pair(pos.first + heebo->moveDY(),
                                    pos.second + heebo->moveDX());
            if (board.contains(newPos) && !heebo->isMovingBack()) {
              Heebo* otherHeebo = board[newPos];
            
              board[newPos] = heebo;
              board[pos] = otherHeebo;
              bool OK = checkSwitch(pos, newPos);
          
              if (!OK) {
                // cout << "STOPPED and moving back" << endl;
                board[pos] = heebo;
                board[newPos] = otherHeebo;
                heebo->moveBack();
                otherHeebo->moveBack();
              } else {
                // cout << "STOPPED and switching" << endl;
                heebo->stopMove();
                otherHeebo->stopMove();
              }
            } else if (!board.contains(newPos) && !heebo->isMovingBack()) {
              // cout << "STOPPED in empty pos" << endl;
              board.erase(pos);
              board[newPos] = heebo;
              heebo->stopMove();
            } else {
              // cout << "STOPPED" << endl;
              heebo->stopMove();
            }        
          } else {
            animRunning = true;
          }
        }
      }
    }
  }

  bool oneMoreChange = false;
  
  // If movement has just stopped
  if (!animRunning && hasChanged) {
    if (fallDown()) {
      animRunning = true;
      oneMoreChange = true;
    } else {
      if (checkForSubsequentHeebos()) {
        animRunning = true;
        oneMoreChange = true;
      }
    }
    if (spawnNewHeebos()) {
      animRunning = true;
      oneMoreChange = true;
    }
  }

  // If anything has changed, redraw the screen
  if (hasChanged) {
    drawScreen();
    if (!oneMoreChange)
      hasChanged = false;
  }

  if (victoryCheck()) {
    if (currentLevel == gameMapSet->numLevels()-1) {
      cout << "VICTORY!" << endl;
    } else {
      currentLevel++;
      initBoard();
      hasChanged = true;
      return;
    }    
  }
  
  // If movement has stopped, check for user input
  SDL_Event event;
  while (SDL_PollEvent(&event)) {
    switch(event.type) {
    case SDL_MOUSEBUTTONUP:
      if (clickX != -1 && clickY != -1) {
        int dx = event.motion.x-clickX;
        int dy = event.motion.y-clickY;

        if (abs(dx) >= MOVE_THRES || abs(dy) >= MOVE_THRES) {
          clickX /= BLOCK_WIDTH;
          clickY /= BLOCK_HEIGHT;
          int toX = clickX;
          int toY = clickY;

          auto clickPos = make_pair(clickY, clickX);

          if (board.contains(clickPos)) {
            if (abs(dx) > abs(dy)) { // left or right movement
              dx = sign(dx);
              dy = 0;
            } else { // up down
              dx = 0;
              dy = sign(dy);
            }
            toX += dx;
            toY += dy;
            
            auto toPos = make_pair(toY, toX);
            Heebo* obj1 = board[clickPos];
            if (board.contains(toPos)) {
              Heebo* obj2 = board[toPos];

              obj1->startMove(dx, dy);
              obj2->startMove(-dx, -dy);
            } else if (!map->isBlocking(toY, toX) && dy == 0) {
              obj1->startMove(dx, dy);
            }
          } 
        }
      }
      clickX = -1;
      clickY = -1;
      break;
    case SDL_MOUSEMOTION:
      if ((event.motion.state & SDL_BUTTON_LMASK) == SDL_BUTTON_LMASK) {
        if (!animRunning && (clickX == -1 || clickY == -1)) {
          clickX = event.motion.x;
          clickY = event.motion.y;
        }
      }
      break;
    }
  }

}

//------------------------------------------------------------------------------

int main() {
  
  // Get SDL version (linked to)
  SDL_version version;
  SDL_GetVersion(&version);
  printf("Using SDL version %u.%u.%u.\n",
          version.major, version.minor, version.patch);

  gameMapSet = new GameMapSet("map.dat", 0);

  // Initialize SDL
  SDL_Init(SDL_INIT_VIDEO);

  // Create window and renderer
  int ret = SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_HEIGHT, 0,
                                        &window, &renderer);
  if (ret != 0)
    return reportError(ret);

  for (const auto& entry : filesystem::directory_iterator("images")) {
    if (!entry.is_regular_file())
      continue;
    filesystem::path p = entry.path();

    SDL_Surface *im = IMG_Load(p.c_str());
    if (!im)
      return reportImageError();

    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, im);
    Sprite s = {texture, im->w, im->h};
    sprites[p.stem()] = s;
    
    SDL_FreeSurface(im);
  }

  ret = TTF_Init();
  if (ret != 0)
    return reportError(ret);
  
  mainFont = TTF_OpenFont("LiberationSans-Regular.ttf", 36);
  if (mainFont == NULL) {
    cout << "Error opening font." << endl;
    return 1;
  }

  initBoard();

  emscripten_set_main_loop(mainLoop, 0, 1);
  //mainLoop();
  
  TTF_CloseFont(mainFont);
  TTF_Quit();
  //SDL_Quit();
  return 0;
}
  
