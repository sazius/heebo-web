#ifndef _HEEBO_H_
#define _HEEBO_H_

#include <string>

using namespace std;

//------------------------------------------------------------------------------

class Heebo {
public:
  explicit Heebo(int type);

  int type() const { return m_type; }
  string typeName() const { return typeName(m_type); }

  void spawnMove();
    
  void startMove(int dx, int dy);
  void stopMove();
  bool moveStep();
  void moveBack();
  bool isMoving() const {
    return m_animDX != 0 || m_animDY != 0;
  }
  bool isMovingBack() const { return m_movingBack; }
  int offsetX() const;
  int offsetY() const;
  int moveDX() const { return m_animDX; }
  int moveDY() const { return m_animDY; }

  void markRemove() { m_doRemove = true; }
  bool toRemove() const { return m_doRemove; }

  static string typeName(int);
  
private:
  int m_type;

  int m_animStep;  // animation step, starts at 0 and moves to
                   // BLOCK_HEIGHT/BLOCK_WIDTH
  int m_animDX;    // rate of movement in X
  int m_animDY;    // ... and Y

  bool m_movingBack;

  bool m_doRemove;
};


#endif /* _HEEBO_H_ */
