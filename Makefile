EMCC      = em++
EMCCFLAGS = -std=c++20 -O0 -Wall 
LDFLAGS   = --shell-file shell_heebo.html
SDLFLAGS  = -sUSE_SDL=2 -sUSE_SDL_IMAGE=2 -sSDL2_IMAGE_FORMATS='["png"]' -sUSE_SDL_TTF=2
PRELOAD   = --preload-file images --preload-file map.dat --preload-file LiberationSans-Regular.ttf

TARGET    = build/index.html
SRCS      = src/main.cpp src/util.cpp src/heebo.cpp src/gamemapset.cpp src/gamemap.cpp
OBJS      = $(SRCS:src/%.cpp=build/%.o)

export PATH := /home/mats/src/emsdk/upstream/bin/:/home/mats/src/emsdk/upstream/emscripten/:$(PATH)

build/%.o: src/%.cpp
	$(EMCC) $(EMCCFLAGS) $(SDLFLAGS) -c -o "$@" "$<"

all: $(TARGET)

$(TARGET): $(OBJS)
	$(EMCC) $(EMCCFLAGS) $(LDFLAGS) $(SDLFLAGS) $(OBJS) -o $(TARGET) $(PRELOAD)

clean:
	rm build/*

upload:
	rsync -var ./build/index.* lakka:www/heebo/
